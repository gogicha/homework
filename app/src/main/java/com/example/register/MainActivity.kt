package com.example.register

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns.*
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth



class MainActivity : AppCompatActivity() {





    private lateinit var mail: EditText
    private lateinit var password1: EditText
    private lateinit var password2: EditText
    private lateinit var register: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        init()
        registerListener()



    }


    private fun init() {

        mail = findViewById(R.id.mail)
        password1 = findViewById(R.id.password1)
        password2 = findViewById(R.id.password2)
        register = findViewById(R.id.register)

    }

    private fun registerListener() {
        register.setOnClickListener {
            val mail = mail.text.toString()
            val password1 = password1.text.toString()
            val password2 = password2.text.toString()




            if (mail.isEmpty() || password1.isEmpty() || password2.isEmpty()) {
                Toast.makeText(this, "Inputs Are Empty!", Toast.LENGTH_SHORT).show()

                return@setOnClickListener

            } else if (!EMAIL_ADDRESS.matcher(mail).matches()) {
                Toast.makeText(this, "E-Mail Is Not Valid", Toast.LENGTH_SHORT).show()

                return@setOnClickListener

            } else if (password1.length < 9) {
                Toast.makeText(this, "Password Must Be At Least 9 Characters!", Toast.LENGTH_SHORT)
                    .show()

                return@setOnClickListener

            } else if (password1 != password2) {
                Toast.makeText(this, "Confirm Password Is Not Matched!", Toast.LENGTH_SHORT).show()

                return@setOnClickListener

            } else if (!password1.matches(".*[!@#$%^&*+=/?].*".toRegex())) { Toast.makeText(this, "Password Must Contain At Least One Symbol", Toast.LENGTH_SHORT).show()

                return@setOnClickListener

            } else if (!password1.matches(".*[0-9].*".toRegex())) {
                Toast.makeText(this, "Password Must Contain At Least One Number", Toast.LENGTH_SHORT).show()

                return@setOnClickListener
            }

            FirebaseAuth.getInstance().createUserWithEmailAndPassword(mail, password1)
                .addOnCompleteListener(){task ->
                    if (task.isSuccessful)
                        startActivity(Intent(this,MainActivity2::class.java))
                    return@addOnCompleteListener

                }.addOnFailureListener {
                    Toast.makeText(this, "Failure!", Toast.LENGTH_SHORT).show()
                    return@addOnFailureListener
                }


        }



    }

}
















